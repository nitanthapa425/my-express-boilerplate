import express, { json } from "express";
import { firstRouter } from "./src/route/firstRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";

let expressApp = express();

connectToMongoDb();
expressApp.use(json()); //it is done to make our application to accept json data
expressApp.use("/", firstRouter);

expressApp.listen(8000, () => {
  console.log("app is listening at port 8000");
});
